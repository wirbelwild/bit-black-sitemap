# Changes in Bit&Black Sitemap v1.1

## 1.1.2 2023-12-05

### Fixed 

-   Ignore data links as well.

## 1.1.1 2022-05-20

### Fixed

-   Fixed bug with ampersands in URLs.