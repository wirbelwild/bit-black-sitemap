<?php

/**
 * Bit&Black Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Sitemap;

use BitAndBlack\Sitemap\Enum\ChangeFreqEnum;
use DateTime;
use DOMDocument;
use DOMException;
use Stringable;

/**
 * Class SitemapXML.
 *
 * @package BitAndBlack\Sitemap
 */
readonly class SitemapXML implements Stringable
{
    private DOMDocument $sitemap;

    /**
     * @param array<string, Page> $websitePages
     * @throws DOMException
     */
    public function __construct(Collection $collection, array $websitePages = [])
    {
        uksort($websitePages, static fn (string|int $itemA, string|int $itemB): int => strnatcasecmp((string) $itemA, (string) $itemB));

        $domDocument = new DOMDocument();
        $domDocument->formatOutput = true;
        $domDocument->encoding = 'utf-8';

        // Set namespace
        $urlset = $domDocument->createElementNS(
            'http://www.sitemaps.org/schemas/sitemap/0.9',
            'urlset'
        );

        // Set image namespace
        $urlset->setAttributeNS(
            'http://www.w3.org/2000/xmlns/',
            'xmlns:image',
            'http://www.google.com/schemas/sitemap-image/1.1'
        );

        // Set link namespace
        $urlset->setAttributeNS(
            'http://www.w3.org/2000/xmlns/',
            'xmlns:xhtml',
            'http://www.w3.org/1999/xhtml'
        );

        $domDocument->appendChild($urlset);

        foreach ($websitePages as $page) {
            $url = $domDocument->createElement('url');
            $urlset->appendChild($url);

            $loc = $domDocument->createElement('loc');
            $loc->nodeValue = htmlspecialchars($page->getUrl());
            $url->appendChild($loc);

            $priority = $domDocument->createElement('priority');
            $priorityValue = $collection->getWebsitePagesPriority($page);
            $priority->nodeValue = (string) (!empty($priorityValue) ? $priorityValue : 1);
            $url->appendChild($priority);

            $lastModification = $domDocument->createElement('lastmod');
            $lastModification->nodeValue = (new DateTime('now'))->format('Y-m-d');
            $url->appendChild($lastModification);
            
            $changeFrequency = $domDocument->createElement('changefreq');
            $changeFrequency->nodeValue = ChangeFreqEnum::DAILY->value;
            $url->appendChild($changeFrequency);

            // Add language versions
            if (array_key_exists($page->getUrl(), $collection->getTreeLanguageCodes())) {
                $relatedPages = $collection->getTreeLanguageCodesValue($page->getUrl());

                foreach ($relatedPages as $languageCode => $relatedPage) {
                    $xhtmlLink = $domDocument->createElement('xhtml:link');

                    $xhtmlLink->setAttribute('rel', 'alternate');
                    $xhtmlLink->setAttribute('hreflang', $languageCode);
                    $xhtmlLink->setAttribute('href', $relatedPage);

                    $url->appendChild($xhtmlLink);
                }
            }

            // Add images
            foreach ($page->getImages() as $imageInformation) {
                $imageImage = $domDocument->createElementNS(
                    'http://www.google.com/schemas/sitemap-image/1.1',
                    'image:image'
                );
                $imageLoc = $domDocument->createElement('image:loc');
                $imageTitle = $domDocument->createElement('image:title');
                
                $imageName = (string) ($imageInformation['src'] ?? '');
                $imageDescription = $imageInformation['title'] ?? $imageInformation['alt'] ?? null;
                
                if ('' === $imageDescription) {
                    $imageDescription = null;
                }

                /**
                 * The replacement of ampersands helps to access URLs like those from Wikipedia,
                 * where ampersands may appear in image names and aren't encoded.
                 */
                $imageName = str_replace('&', urlencode('&'), $imageName);

                $imageLoc->nodeValue = $imageName;

                $imageTitle->nodeValue = htmlspecialchars((string) ($imageDescription ?? $this->getImageTitleFromImageName($imageName)));

                $imageImage->appendChild($imageLoc);
                $imageImage->appendChild($imageTitle);

                $url->appendChild($imageImage);
            }
        }

        $this->sitemap = $domDocument;
    }

    /**
     * @return DOMDocument
     */
    public function getSitemap(): DOMDocument
    {
        return $this->sitemap;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string) $this->getSitemap()->saveXML();
    }

    /**
     * @return string
     */
    private function getImageTitleFromImageName(string $imageName): string
    {
        $pathInfo = pathinfo($imageName);

        $title = str_replace(
            '-',
            ' ',
            isset($pathInfo['extension'])
                ? basename($imageName, '.' . $pathInfo['extension'])
                : basename($imageName)
        );
            
        return mb_convert_case($title, MB_CASE_TITLE, 'UTF-8');
    }
}
