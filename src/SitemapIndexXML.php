<?php

/**
 * Bit&Black Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Sitemap;

use DOMDocument;
use Stringable;

/**
 * Class SitemapIndexXML
 *
 * @package BitAndBlack\Sitemap
 */
class SitemapIndexXML implements Stringable
{
    private readonly DOMDocument $sitemap;

    /**
     * SitemapIndexXML constructor.
     *
     * @param array<string, string> $pathLanguageVersions
     */
    public function __construct(array $pathLanguageVersions)
    {
        $domDocument = new DOMDocument();
        $domDocument->formatOutput = true;
        $domDocument->encoding = 'utf-8';

        $sitemapindex = $domDocument->createElement('sitemapindex');

        $sitemapindex->setAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');

        foreach ($pathLanguageVersions as $pathLanguageVersion) {
            $sitemap = $domDocument->createElement('sitemap');

            $loc = $domDocument->createElement('loc');
            $loc->nodeValue = $pathLanguageVersion;

            $lastmod = $domDocument->createElement('lastmod');
            $lastmod->nodeValue = date(DATE_ATOM);

            $sitemap->appendChild($loc);
            $sitemap->appendChild($lastmod);

            $sitemapindex->appendChild($sitemap);
        }

        $domDocument->appendChild($sitemapindex);
        $this->sitemap = $domDocument;
    }

    /**
     * @return DOMDocument
     */
    public function getSitemap(): DOMDocument
    {
        return $this->sitemap;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string) $this->getSitemap()->saveXML();
    }
}
