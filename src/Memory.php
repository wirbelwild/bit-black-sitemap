<?php

/**
 * Bit&Black Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Sitemap;

use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * Class Memory
 *
 * @package BitAndBlack\Sitemap
 * @see \BitAndBlack\Sitemap\Tests\MemoryTest
 */
class Memory implements LoggerAwareInterface
{
    private LoggerInterface $logger;

    /**
     * Memory constructor.
     */
    public function __construct()
    {
        $this->logger = new NullLogger();
    }

    /**
     * Checks if enough memory is available.
     *
     * @return bool
     */
    public function hasEnoughMemory(): bool
    {
        $memoryLimit = (string) ini_get('memory_limit');
        $memorySentence = 'Memory limit: Not existing';

        $hasEnoughMemory = true;

        $memoryUsage = memory_get_usage();
        $memoryUsage = (($memoryUsage / 1024) / 1024);
        
        if ('-1' !== $memoryLimit) {
            $memoryLimit = ($this->returnBytes($memoryLimit) / 1024) / 1024;
            $buffer = $memoryLimit / 2;
            $hasEnoughMemory = $memoryLimit > ($memoryUsage + $buffer);
            $memorySentence = 'Memory limit: ' . var_export($memoryLimit, true) . 'MB';
        }
        
        $this->logger->debug(
            $memorySentence .
            ', Memory in use: ' . var_export(round($memoryUsage, 2), true) . 'MB, ' .
            round(($memoryUsage / $memoryLimit) * 100, 2) . '%, '
        );

        return $hasEnoughMemory;
    }

    /**
     * @return float
     */
    public function returnBytes(string $value): float
    {
        $value = trim($value);
        $last = mb_strtolower($value[strlen($value) - 1]);
        $val = (float) $value;

        switch ($last) {
            case 'g':
                $val *= 1024;

                // no break
            case 'm':
                $val *= 1024;

                // no break
            case 'k':
                $val *= 1024;
        }

        if ($val < 0) {
            return 0;
        }

        return $val;
    }

    /**
     * @param LoggerInterface $logger
     * @return void
     */
    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }
}
