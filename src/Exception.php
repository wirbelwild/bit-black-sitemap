<?php

/**
 * Bit&Black Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Sitemap;

use Throwable;

/**
 * Class Exception
 *
 * @package BitAndBlack\Sitemap
 */
class Exception extends \Exception
{
    /**
     * Exception constructor.
     *
     * @param Throwable|null $previous
     */
    public function __construct(string $message = '', int $code = 0, Throwable|null $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
