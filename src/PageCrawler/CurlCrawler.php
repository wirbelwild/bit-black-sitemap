<?php

/**
 * Bit&Black Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Sitemap\PageCrawler;

use BitAndBlack\Sitemap\Exception\DependencyNotInstalledException;
use Http\Discovery\Psr17Factory;
use Psr\Http\Message\ResponseInterface;

/**
 * Class CURLCrawler.
 *
 * @package BitAndBlack\Sitemap\PageCrawler
 * @see \BitAndBlack\Sitemap\Tests\PageCrawler\CurlCrawlerTest
 */
class CurlCrawler implements PageCrawlerInterface
{
    /**
     * @throws DependencyNotInstalledException
     */
    public function __construct()
    {
        if (!self::isAvailable()) {
            throw new DependencyNotInstalledException('ext-curl');
        }
    }

    public function requestUrl(string $url): ResponseInterface
    {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_REFERER, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = [];

        curl_setopt(
            $curl,
            CURLOPT_HEADERFUNCTION,
            static function ($curl, $header) use (&$headers) {
                $length = strlen($header);
                $header = explode(':', $header, 2);

                if (count($header) < 2) {
                    return $length;
                }

                $key = strtolower(trim($header[0]));
                $value = trim($header[1]);

                $headers[$key][] = $value;

                return $length;
            }
        );

        $urlContent = (string) curl_exec($curl);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        $psr17Factory = new Psr17Factory();

        $response = $psr17Factory
            ->createResponse($httpCode)
            ->withBody(
                $psr17Factory->createStream($urlContent)
            )
        ;

        foreach ($headers as $headerName => $headerValue) {
            $response = $response->withHeader($headerName, $headerValue);
        }

        return $response;
    }

    public static function isAvailable(): bool
    {
        return in_array('curl', get_loaded_extensions());
    }
}
