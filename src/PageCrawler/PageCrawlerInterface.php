<?php

/**
 * Bit&Black Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Sitemap\PageCrawler;

use Psr\Http\Message\ResponseInterface;

/**
 * Interface PageCrawlerInterface.
 *
 * @package BitAndBlack\Sitemap\PageCrawler
 */
interface PageCrawlerInterface
{
    /**
     * @return ResponseInterface
     */
    public function requestUrl(string $url): ResponseInterface;

    public static function isAvailable(): bool;
}
