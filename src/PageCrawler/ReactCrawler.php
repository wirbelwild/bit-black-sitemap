<?php

/**
 * Bit&Black Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Sitemap\PageCrawler;

use BitAndBlack\Composer\Composer;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface;
use React\Http\Browser;
use React\Http\Message\Response;
use Throwable;
use function React\Async\await;

/**
 * @see \BitAndBlack\Sitemap\Tests\PageCrawler\ReactCrawlerTest
 */
class ReactCrawler implements PageCrawlerInterface
{
    /**
     * @inheritDoc
     */
    public function requestUrl(string $url): ResponseInterface
    {
        $browser = new Browser();
        $promise = $browser->get($url);

        try {
            $response = await($promise);
        } catch (Throwable) {
            $response = new Response(
                StatusCodeInterface::STATUS_INTERNAL_SERVER_ERROR
            );
        }

        return $response;
    }

    public static function isAvailable(): bool
    {
        return Composer::classExists(Browser::class);
    }
}
