<?php

/**
 * Bit&Black Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Sitemap\PageCrawler;

use BitAndBlack\Sitemap\Exception\NoPageCrawlerAvailableException;
use Psr\Http\Message\ResponseInterface;

class AutoPageCrawler implements PageCrawlerInterface
{
    /**
     * @var array<int, class-string<PageCrawlerInterface>>
     */
    private array $pageCrawlers = [
        SymfonyCrawler::class,
        GuzzleCrawler::class,
        ReactCrawler::class,
        CurlCrawler::class,
        BrowserShotCrawler::class,
    ];

    private ?object $pageCrawler = null;

    /**
     * @throws NoPageCrawlerAvailableException
     */
    public function __construct()
    {
        foreach ($this->pageCrawlers as $pageCrawler) {
            if (!is_callable($pageCrawler . '::isAvailable')) {
                continue;
            }

            if (!call_user_func($pageCrawler . '::isAvailable')) {
                continue;
            }

            $this->pageCrawler = new $pageCrawler();
            break;
        }

        if (null === $this->pageCrawler) {
            throw new NoPageCrawlerAvailableException();
        }
    }

    /**
     * @inheritDoc
     * @throws NoPageCrawlerAvailableException
     */
    public function requestUrl(string $url): ResponseInterface
    {
        if (!$this->pageCrawler instanceof PageCrawlerInterface) {
            throw new NoPageCrawlerAvailableException();
        }

        return $this->pageCrawler->requestUrl($url);
    }

    public static function isAvailable(): bool
    {
        return true;
    }
}
