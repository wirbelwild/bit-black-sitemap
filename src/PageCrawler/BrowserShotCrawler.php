<?php

/**
 * Bit&Black Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Sitemap\PageCrawler;

use BitAndBlack\Composer\Composer;
use BitAndBlack\Sitemap\Exception\DependencyNotInstalledException;
use Http\Discovery\Psr17Factory;
use Psr\Http\Message\ResponseInterface;
use Spatie\Browsershot\Browsershot;
use Spatie\Browsershot\Exceptions\FileUrlNotAllowed;

/**
 * Class BrowserShotCrawler.
 *
 * @package BitAndBlack\Sitemap\PageCrawler
 * @see \BitAndBlack\Sitemap\Tests\PageCrawler\BrowserShotCrawlerTest
 */
readonly class BrowserShotCrawler implements PageCrawlerInterface
{
    /**
     * @throws DependencyNotInstalledException
     */
    public function __construct(
        private ?PageCrawlerInterface $additionalPageCrawler = null,
    ) {
        if (!self::isAvailable()) {
            throw new DependencyNotInstalledException('spatie/browsershot');
        }
    }

    public static function isAvailable(): bool
    {
        return Composer::classExists(Browsershot::class);
    }

    /**
     * @throws FileUrlNotAllowed
     */
    public function requestUrl(string $url): ResponseInterface
    {
        $browserShot = new Browsershot();
        $browserShot
            ->userAgent('Kiwa Sitemap Crawler')
            ->waitUntilNetworkIdle()
            ->timeout(12)
        ;

        $body = $browserShot->setUrl($url)->bodyHtml();

        $psr17Factory = new Psr17Factory();

        $response = $psr17Factory
            ->createResponse()
            ->withBody(
                $psr17Factory->createStream($body)
            )
        ;

        if (null !== $this->additionalPageCrawler) {
            $response = $this->additionalPageCrawler->requestUrl($url);
            $response->withBody(
                $psr17Factory->createStream($body)
            );
        }

        return $response;
    }
}
