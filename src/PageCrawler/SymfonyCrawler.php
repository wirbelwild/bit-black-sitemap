<?php

/**
 * Bit&Black Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Sitemap\PageCrawler;

use BitAndBlack\Composer\Composer;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpClient\Psr18Client;

/**
 * @see \BitAndBlack\Sitemap\Tests\PageCrawler\SymfonyCrawlerTest
 */
class SymfonyCrawler implements PageCrawlerInterface
{
    /**
     * @throws ClientExceptionInterface
     */
    public function requestUrl(string $url): ResponseInterface
    {
        $client = new Psr18Client();
        $request = $client->createRequest('GET', $url);
        return $client->sendRequest($request);
    }

    public static function isAvailable(): bool
    {
        return Composer::classExists(HttpClient::class);
    }
}
