<?php

/**
 * Bit&Black Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Sitemap\PageCrawler;

use BitAndBlack\Composer\Composer;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;

/**
 * @see \BitAndBlack\Sitemap\Tests\PageCrawler\GuzzleCrawlerTest
 */
class GuzzleCrawler implements PageCrawlerInterface
{
    public function requestUrl(string $url): ResponseInterface
    {
        $client = new Client();

        try {
            $response = $client->get(
                $url,
                [
                    'allow_redirects' => false,
                    'http_errors' => false,
                ]
            );
        } catch (GuzzleException) {
            $response = new Response(500);
        }

        return $response;
    }

    public static function isAvailable(): bool
    {
        return Composer::classExists(Client::class);
    }
}
