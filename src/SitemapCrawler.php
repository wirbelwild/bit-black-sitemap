<?php

/**
 * Bit&Black Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Sitemap;

use BitAndBlack\Sitemap\Config\ConfigInterface;
use BitAndBlack\Sitemap\Exception\NoPageCrawlerAvailableException;
use BitAndBlack\Sitemap\Writer\WriterInterface;
use DOMException;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Handles websites' sitemaps
 * @see \BitAndBlack\Sitemap\Tests\SitemapCrawlerTest
 */
class SitemapCrawler implements LoggerAwareInterface
{
    final public const STATUS_PENDING = 'pending';
    final public const STATUS_FINISHED = 'finished';
    final public const STATUS_ERROR = 'error';
    
    /**
     * main url
     */
    private ?string $mainURL = null;
    
    /**
     * How long the script is allowed to run in seconds
     */
    private int $maxExecutionTime;

    /**
     * Timestamp when the execution started
     */
    private ?int $executionStartTime = null;
    
    /**
     * @var array<int, string>
     */
    private array $xmlFiles;

    private readonly string $lastExecutionTime;

    private LoggerInterface $logger;

    private OutputInterface $output;
    
    private readonly Memory $memory;
    
    private readonly Collection $collection;
    
    private UrlProcessor $urlProcessor;
    
    private int $crawlingLimit = -1;

    /**
    * Sitemap constructor.
    *
     * @param ConfigInterface $config Instance of the config object. This file is needed to store information about
     *                                the crawling. Huge websites can take some time to crawl, the sitemap crawler
     *                                will stop when a memory limit or a time limit has been reached and can continue
     *                                the next time it is getting executed at this place.
     * @param WriterInterface $writer Instance of the writer object. This should be for normal the
                                      FileWriter which allows to write the xml files to disk.
    */
    public function __construct(
        private readonly ConfigInterface $config,
        private readonly WriterInterface $writer,
    ) {
        $this->logger = new NullLogger();
        $this->output = new NullOutput();
        $this->memory = new Memory();
        
        // Get max execution time from server
        $maxExecutionTime = (int) @ini_get('max_execution_time');
        $this->maxExecutionTime = $maxExecutionTime;

        if (0 === $maxExecutionTime) {
            $this->maxExecutionTime = 100;
        }
        
        $this->collection = new Collection($config);

        $lastExecutionTime = $config['lastExecutionTime'] ?? '';

        if (!is_string($lastExecutionTime)) {
            $lastExecutionTime = '';
        }

        $this->lastExecutionTime = $lastExecutionTime;

        $xmlFiles = $config['xmlFiles'] ?? [];

        if (!is_array($xmlFiles)) {
            $xmlFiles = [];
        }

        $this->xmlFiles = $xmlFiles;

        $this->urlProcessor = new UrlProcessor('');
    }

    /**
     * Start crawling
     *
     * @param string $url The url to start with.
     * @return array<string, mixed>
     * @throws DOMException
     */
    public function createSitemap(string $url): array
    {
        $this->output->writeln('Maximum execution time defined with ' . $this->maxExecutionTime . ' seconds');
        $this->logger->debug('Maximum execution time defined with ' . $this->maxExecutionTime . ' seconds');

        $url = trim($url, '/#');
        $this->logger->debug('Root URL is ' . $url);
        $this->output->writeln('Root URL is <info>' . $url . '</info>');

        $this->mainURL = $url;
        $this->urlProcessor = new UrlProcessor($this->mainURL);
        
        if (empty($this->collection->getUrlsToProcess())) {
            $this->collection->addUrlToProcess($url);
        }

        // Set execution start time
        if (null === $this->executionStartTime) {
            $this->executionStartTime = time();
        }
        
        // Loop crawler as long as time limit is not reached
        while (true) {
            $this->logger->debug('There are ' . count($this->collection->getUrlsToProcess()) . ' URLs left.');
            $url = $this->collection->getUrlsToProcessValue(0);

            $this->exeWebsiteCrawling($url);

            $hasTime = time() < ($this->executionStartTime + $this->maxExecutionTime);
            $hasURLsToProcess = !empty($this->collection->getUrlsToProcess());
            $hasEnoughMemory = $this->memory->hasEnoughMemory();
            $hasPageLimitReached = $this->hasPageLimitReached();

            $mayContinue = $hasTime && $hasURLsToProcess && $hasEnoughMemory && !$hasPageLimitReached;

            if (!$mayContinue) {
                break;
            }
        }

        $hasPageLimitReached = $this->hasPageLimitReached();
        
        $stopMessage = 'Stopped parsing because of time limit';
        
        if (!$this->memory->hasEnoughMemory()) {
            $stopMessage = 'Stopped parsing because of memory limit';
        } elseif (empty($this->collection->getUrlsToProcess())) {
            $stopMessage = 'No more URLs to process';
        } elseif ($hasPageLimitReached) {
            $stopMessage = 'Stopped parsing because of page count limit';
        }
        
        $this->output->writeln($stopMessage);
        $this->logger->debug($stopMessage);

        // Crawling not finished
        // Save crawling data to file
        if (false === $hasPageLimitReached
            && !empty($this->collection->getUrlsToProcess())
        ) {
            $this->config->offsetSet('URLSToProcess', $this->collection->getUrlsToProcess());
            $this->config->offsetSet('seen', $this->collection->getSeen());
            $this->config->offsetSet('tree', $this->collection->getTree());
            $this->config->offsetSet('treePriority', $this->collection->getTreePriority());
            $this->config->offsetSet('treePages', $this->collection->getTreePages());
            $this->config->offsetSet('treeLanguageCodes', $this->collection->getTreeLanguageCodes());
            $this->config->offsetSet('lastExecutionTime', $this->lastExecutionTime);
            $this->config->offsetSet('xmlFiles', $this->xmlFiles);
            $this->config->offsetSet('pages', $this->collection->getPages());

            $this->writer->setConfig($this->config);

            return [
                'status' => self::STATUS_PENDING,
                'pages' => count($this->collection->getSeen()),
                'pagesLeft' => count($this->collection->getUrlsToProcess()),
            ];
        }

        $this->collection->createPagesPriority();

        // Crawling is finished
        if ($this->saveSitemap()) {
            $this->config->offsetUnset('URLSToProcess');
            $this->config->offsetUnset('seen');
            $this->config->offsetUnset('tree');
            $this->config->offsetUnset('treeImages');
            $this->config->offsetUnset('treePriority');
            $this->config->offsetUnset('treePages');
            $this->config->offsetUnset('treeLanguageCodes');
            $this->config->offsetSet('lastExecutionTime', date('Y-m-d H:i:s'));
            $this->config->offsetSet('xmlFiles', $this->xmlFiles);
            $this->config->offsetUnset('pages');

            $this->writer->setConfig($this->config);
           
            return [
                'status' => self::STATUS_FINISHED,
                'pages' => count($this->collection->getSeen()),
            ];
        }

        return [
            'status' => self::STATUS_ERROR,
        ];
    }

    /**
     * Crawls all website pages
     *
     * @return void
     * @throws NoPageCrawlerAvailableException
     */
    private function exeWebsiteCrawling(string $url): void
    {
        $this->logger->debug('Handling url ' . $url);
        $this->output->writeln('Handling url <info>' . $url . '</info>');

        // Set url to seen urls
        if (in_array($url, $this->collection->getSeen(), true)) {
            $this->removeFromProcessList($url);
            $this->logger->debug(var_export($url, true) . ' has been seen');
            return;
        }

        $this->collection->addSeen($url);
        
        $pageCrawler = new PageCrawler($url);
        $page = $pageCrawler->getPage();

        $this->collection->addPage($page);

        if (404 === $page->getStatusCode()) {
            $this->logger->debug(var_export($url, true) . ' is dead');
            $this->removeFromProcessList($url);
            return;
        }

        if (null !== $redirection = $page->getRedirection()) {
            if (null === URLParser::parse($redirection)['host']) {
                $redirection = $this->mainURL . URLParser::parse($redirection)['path'];
            }

            $this->logger->debug('Found location ' . var_export($redirection, true));

            if (in_array($redirection, $this->collection->getSeen(), true)) {
                return;
            }

            $this->logger->debug('Added location to list');
            $this->collection->addUrlToProcess($redirection);
        }

        if ($page->isAuthenticationRequired()) {
            $this->logger->debug('This url requires authentication');
        }

        // Ignore wrong links
        if ($this->urlProcessor->isUrlIgnored($url)) {
            $this->logger->debug(var_export($url, true) . ' is getting ignored');
            return;
        }

        // Ignore external links
        if ($this->urlProcessor->isUrlExternal($url)) {
            $this->logger->debug(var_export($url, true) . ' is external');
            return;
        }
        
        // Get language code of page
        if ([] !== $links = $page->getHrefLangs()) {
            $this->logger->debug(count($links) . ' links found');

            foreach ($links as $languageCode => $link) {
                $this->collection->addTreeLanguageCodes($url, $languageCode, $link);
            }
        }

        // Search for language code in html tag when no alternate links found
        if (null !== $languageCode = $page->getLanguageCode()) {
            $this->collection->addTreeLanguageCodes($url, $languageCode, $url);
        }

        // Get all a tags
        $anchors = $page->getLinks();

        $this->logger->debug(count($anchors) . ' anchors found');

        foreach (array_keys($anchors) as $href) {
            // If is valid page
            if (!$this->urlProcessor->isUrlExternal($href)
                && !$this->urlProcessor->isUrlIgnored($href)
            ) {
                // Create an array when not existing
                if (!array_key_exists($url, $this->collection->getTree())) {
                    $this->collection->setTreeValue($url);
                }

                // Append to array when not existing
                if (!in_array($href, $this->collection->getTreeValue($url), true)) {
                    $this->collection->setTreeValue($url, $href);
                }

                // Follow a link when it's a valid url
                if (!$this->urlProcessor->isUrlImage($href)
                    && !in_array($href, $this->collection->getUrlsToProcess(), true)
                    && !in_array($href, $this->collection->getSeen(), true)
                ) {
                    $this->collection->addUrlToProcess($href);
                }
            }
        }

        // Add to a treepages array

        /**
         * This is currently needed as PHPStan thinks this would always be false.
         *
         * @var bool $isUrlIgnored
         */
        $isUrlIgnored = $this->urlProcessor->isUrlIgnored($url);
        $isUrlImage = $this->urlProcessor->isUrlImage($url);

        if (!$isUrlIgnored && !$isUrlImage) {
            $parsedUrl = URLParser::parse($url);
            $mayBeHandled = false;
            
            if (empty($parsedUrl['query'])) {
                $mayBeHandled = true;
            }
            
            // Append to array when not existing
            if ($mayBeHandled && !in_array($url, $this->collection->getTreePages(), true)) {
                $this->collection->addTreePages($url);
            }
        }

        // Delete from the list
        $this->removeFromProcessList($url);
    }

    /**
     * Removes a URL from the processing list.
     *
     * @return bool
     */
    private function removeFromProcessList(string $url): bool
    {
        if (false !== ($key = array_search($url, $this->collection->getUrlsToProcess(), true))) {
            $this->collection->unsetUrlsToProcessValue($key);
            return true;
        }
        
        return false;
    }

    /**
     * Saves all sitemap files to root.
     *
     * @return bool
     * @throws DOMException
     */
    private function saveSitemap(): bool
    {
        foreach ($this->xmlFiles as $xmlFile) {
            $path = $this->writer->getRootDir() . DIRECTORY_SEPARATOR . $xmlFile;

            if (file_exists($path)) {
                unlink($path);
            }
        }

        $this->xmlFiles = [];

        $languageVersions = $this->collection->getLanguageVersions();
        $pathLanguageVersions = [];

        foreach ($languageVersions as $languageCode => $websitePages) {
            $sitemap = new SitemapXML(
                $this->collection,
                $websitePages
            );

            $pathLanguageVersion = 'sitemap-' . $languageCode . '.xml';
            $pathLanguageVersions[$languageCode] = $this->mainURL . '/' . $pathLanguageVersion;

            $this->writer->addSitemap($pathLanguageVersion, $sitemap);
            $this->xmlFiles[] = $pathLanguageVersion;
        }

        $mainSitemap = new SitemapIndexXML($pathLanguageVersions);
        $this->writer->addSitemapIndex('sitemap.xml', $mainSitemap);

        return true;
    }

    /**
     * @param LoggerInterface $logger
     * @return void
     */
    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
        $this->memory->setLogger($logger);
    }

    /**
     * @return $this
     */
    public function setOutput(OutputInterface $output): self
    {
        $this->output = $output;
        return $this;
    }

    /**
     * Sets a limit of how many pages should get crawled.
     *
     * @return $this
     */
    public function setCrawlingLimit(int $pageCount): self
    {
        $this->crawlingLimit = $pageCount;
        return $this;
    }

    private function hasPageLimitReached(): bool
    {
        $limit = $this->crawlingLimit;

        if (0 >= $limit) {
            return false;
        }

        $seen = count($this->collection->getSeen());

        return $seen >= $limit;
    }
}
