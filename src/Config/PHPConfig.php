<?php

/**
 * Bit&Black Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Sitemap\Config;

use Stringable;

/**
 * Class PHPConfig
 *
 * @package BitAndBlack\Sitemap\Config
 */
class PHPConfig extends AbstractConfig implements ConfigInterface, Stringable
{
    /**
     * PHPConfig constructor.
     *
     * @param string|null $path
     */
    public function __construct(
        private readonly ?string $path = null,
    ) {
        if (null !== $path && file_exists($path)) {
            $this->config = include $path;
        }
    }

    /**
     * @return string
     */
    public function getConfig(): string
    {
        return '<?php return ' . var_export($this->config, true) . ';';
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getConfig();
    }

    /**
     * @return string|null
     */
    public function getPath(): ?string
    {
        return $this->path;
    }
}
