<?php

/**
 * Bit&Black Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Sitemap\Config;

use ReturnTypeWillChange;

/**
 * Class AbstractConfig
 *
 * @package BitAndBlack\Sitemap\Config
 */
abstract class AbstractConfig
{
    /**
     * @var array<mixed>
     */
    protected $config = [];

    /**
     * @return bool
     */
    public function offsetExists(mixed $offset): bool
    {
        return isset($this->config[$offset]);
    }

    /**
     * @return mixed|null
     */
    #[ReturnTypeWillChange]
    public function offsetGet(mixed $offset)
    {
        return $this->config[$offset] ?? null;
    }

    public function offsetSet(mixed $offset, mixed $value): void
    {
        if (null === $offset) {
            $this->config[] = $value;
        } else {
            $this->config[$offset] = $value;
        }
    }

    public function offsetUnset(mixed $offset): void
    {
        unset($this->config[$offset]);
    }
}
