<?php

/**
 * Bit&Black Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Sitemap\Config;

use Stringable;
use Symfony\Component\Yaml\Yaml;

/**
 * Class YamlConfig
 *
 * @package BitAndBlack\Sitemap\Config
 */
class YamlConfig extends AbstractConfig implements ConfigInterface, Stringable
{
    /**
     * YamlConfig constructor.
     *
     * @param string|null $path
     */
    public function __construct(private readonly ?string $path = null)
    {
        if (null !== $path && file_exists($path)) {
            $config = Yaml::parseFile($path, Yaml::PARSE_OBJECT);

            if (!is_array($config)) {
                $config = [];
            }

            $this->config = $config;
        }
    }

    /**
     * @return string
     */
    public function getConfig(): string
    {
        return Yaml::dump($this->config, 4, 4, Yaml::DUMP_OBJECT);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getConfig();
    }

    /**
     * @return string|null
     */
    public function getPath(): ?string
    {
        return $this->path;
    }
}
