<?php

/**
 * Bit&Black Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Sitemap\Config;

use ArrayAccess;

/**
 * Interface ConfigInterface
 *
 * @package BitAndBlack\Sitemap\Config
 * @extends ArrayAccess<string, mixed>
 */
interface ConfigInterface extends ArrayAccess
{
    /**
     * @return string
     */
    public function getConfig(): string;
    
    /**
     * @return string|null
     */
    public function getPath(): ?string;
}
