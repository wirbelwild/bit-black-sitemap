<?php

/**
 * Bit&Black Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Sitemap;

use BitAndBlack\Sitemap\Config\ConfigInterface;

/**
 * Class Collection
 *
 * @package BitAndBlack\Sitemap
 */
class Collection
{
    /**
     * Array of pages and all links
     * Current URL => Link URL
     *
     * @var array<string, array<int, string>>
     */
    private array $tree = [];

    /**
     * Array of pages with their priority
     * Current URL => priority
     *
     * @var array<string, float>
     */
    private array $treePriority = [];

    /**
     * Array of all pages
     *
     * @var array<int, string>
     */
    private array $treePages = [];

    /**
     * Array of pages and language codes
     * Current URL => Language Code of Link => Link URL
     *
     * @var array<string, array<string, string>>
     */
    private array $treeLanguageCodes = [];

    /**
     * Array of seen urls
     *
     * @var array<int, string>
     */
    private array $seen = [];

    /**
     * URLs that need to be crawled
     *
     * @var array<int, string>
     */
    private array $urlsToProcess = [];

    /**
     * @var array<string, Page>
     */
    private array $pages = [];

    /**
     * Collection constructor.
     */
    public function __construct(ConfigInterface $config)
    {
        if (isset($config['URLSToProcess']) && !empty($config['URLSToProcess']) && is_array($config['URLSToProcess'])) {
            $this->setUrlsToProcess($config['URLSToProcess']);
        }

        if (isset($config['seen']) && is_array($config['seen'])) {
            $this->setSeen($config['seen']);
        }

        if (isset($config['tree']) && is_array($config['tree'])) {
            $this->setTree($config['tree']);
        }

        if (isset($config['treePriority']) && is_array($config['treePriority'])) {
            $this->setTreePriority($config['treePriority']);
        }

        if (isset($config['treePages']) && is_array($config['treePages'])) {
            $this->setTreePages($config['treePages']);
        }

        if (isset($config['treeLanguageCodes']) && is_array($config['treeLanguageCodes'])) {
            $this->setTreeLanguageCodes($config['treeLanguageCodes']);
        }

        if (isset($config['pages']) && is_array($config['pages'])) {
            $this->pages = $config['pages'];
        }
    }

    /**
     * @return array<string, array<int, string>>
     */
    public function getTree(): array
    {
        return $this->tree;
    }

    /**
     * @param array<string, array<int, string>> $tree
     * @return Collection
     */
    public function setTree(array $tree): Collection
    {
        $this->tree = $tree;
        return $this;
    }

    /**
     * @return array<string, float>
     */
    public function getTreePriority(): array
    {
        return $this->treePriority;
    }

    /**
     * @param array<string, float> $treePriority
     * @return Collection
     */
    public function setTreePriority(array $treePriority): Collection
    {
        $this->treePriority = $treePriority;
        return $this;
    }

    /**
     * @return array<int, string>
     */
    public function getTreePages(): array
    {
        return $this->treePages;
    }

    /**
     * @param array<int, string> $treePages
     * @return Collection
     */
    public function setTreePages(array $treePages): Collection
    {
        $this->treePages = $treePages;
        return $this;
    }

    /**
     * @return array<string, array<string, string>>
     */
    public function getTreeLanguageCodes(): array
    {
        return $this->treeLanguageCodes;
    }

    /**
     * @param array<string, array<string, string>> $treeLanguageCodes
     * @return Collection
     */
    public function setTreeLanguageCodes(array $treeLanguageCodes): Collection
    {
        $this->treeLanguageCodes = $treeLanguageCodes;
        return $this;
    }

    /**
     * @return array<int, string>
     */
    public function getSeen(): array
    {
        return $this->seen;
    }

    /**
     * @param array<int, string> $seen
     * @return Collection
     */
    public function setSeen(array $seen): Collection
    {
        $this->seen = $seen;
        return $this;
    }

    /**
     * @return array<int, string>
     */
    public function getUrlsToProcess(): array
    {
        return $this->urlsToProcess;
    }

    /**
     * @param array<int, string> $urlsToProcess
     * @return Collection
     */
    public function setUrlsToProcess(array $urlsToProcess): Collection
    {
        $this->urlsToProcess = $urlsToProcess;
        return $this;
    }

    /**
     * @return Collection
     */
    public function addUrlToProcess(string $url): self
    {
        if (!in_array($url, $this->urlsToProcess, true)) {
            $this->urlsToProcess[] = $url;
        }
        return $this;
    }

    /**
     * @return $this
     */
    public function addSeen(string $url): self
    {
        $this->seen[] = $url;
        return $this;
    }

    /**
     * @return $this
     */
    public function addTreeLanguageCodes(string $url, string $hreflang, string $href): self
    {
        $this->treeLanguageCodes[$url][$hreflang] = $href;
        return $this;
    }

    /**
     * @return $this
     */
    public function addTreePages(string $url): self
    {
        $this->treePages[] = $url;
        return $this;
    }

    /**
     * @return float
     */
    public function getTreePriorityValue(string $url): float
    {
        return $this->treePriority[$url];
    }

    /**
     * @return $this
     */
    public function updateTreePriority(string $url, float $value): self
    {
        $this->treePriority[$url] = $value;
        return $this;
    }

    /**
     * @return array<string, string>
     */
    public function getTreeLanguageCodesValue(string $url): array
    {
        return $this->treeLanguageCodes[$url];
    }

    /**
     * @return string
     */
    public function getUrlsToProcessValue(int $key): string
    {
        return $this->urlsToProcess[$key];
    }

    /**
     * @return $this
     */
    public function unsetUrlsToProcessValue(int $key): self
    {
        unset($this->urlsToProcess[$key]);
        $this->urlsToProcess = array_values($this->urlsToProcess);
        return $this;
    }

    /**
     * @param string|null $value
     * @return $this
     */
    public function setTreeValue(string $url, string|null $value = null): self
    {
        if (null === $value) {
            $this->tree[$url] = [];
            return $this;
        }
        
        $this->tree[$url][] = $value;
        return $this;
    }

    /**
     * @return array<int, string>
     */
    public function getTreeValue(string $url): array
    {
        return $this->tree[$url];
    }

    /**
     * @return float
     */
    public function getWebsitePagesPriority(Page $page): float
    {
        $url = $page->getUrl();

        if (array_key_exists($url, $this->getTreePriority())) {
            return $this->getTreePriorityValue($url);
        }

        return 1;
    }

    /**
     * @return $this
     */
    public function createPagesPriority(): self
    {
        foreach ($this->getTree() as $links) {
            foreach ($links as $link) {
                if (!array_key_exists($link, $this->getTreePriority())) {
                    $this->updateTreePriority($link, 1);
                    continue;
                }

                $this->updateTreePriority(
                    $link,
                    $this->getTreePriorityValue($link) + 1
                );
            }
        }

        // Sort array
        $treePriority = $this->getTreePriority();
        asort($treePriority);
        $highest = end($treePriority);
        arsort($treePriority);
        $this->setTreePriority($treePriority);

        // Change values to numbers between 0 and 1
        foreach ($this->getTreePriority() as $urlKey => $priority) {
            $this->updateTreePriority($urlKey, round($priority / $highest, 4));
        }

        return $this;
    }

    /**
     * Return pages sorted by language
     *
     * @return array<string, array<string, Page>>
     */
    public function getLanguageVersions(): array
    {
        $allLanguageCodes = [];

        foreach ($this->getTreeLanguageCodes() as $languageVersions) {
            foreach ($languageVersions as $languageCode => $url) {
                if (!array_key_exists($languageCode, $allLanguageCodes)) {
                    $allLanguageCodes[$languageCode] = [];
                }

                if (!in_array($url, $allLanguageCodes[$languageCode], false)) {
                    $allLanguageCodes[$languageCode][$url] = $this->pages[$url];
                }
            }
        }

        return $allLanguageCodes;
    }

    public function addPage(Page $page): self
    {
        $this->pages[$page->getUrl()] = $page;
        return $this;
    }

    /**
     * @return array<string, Page>
     */
    public function getPages(): array
    {
        return $this->pages;
    }
}
