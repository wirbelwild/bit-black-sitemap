<?php

/**
 * Bit&Black Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Sitemap\Writer;

use BitAndBlack\Sitemap\Config\ConfigInterface;
use BitAndBlack\Sitemap\SitemapIndexXML;
use BitAndBlack\Sitemap\SitemapXML;

/**
 * Class StringWriter
 *
 * @package BitAndBlack\Sitemap\Writer
 */
class StringWriter implements WriterInterface
{
    private ?ConfigInterface $config = null;

    /**
     * @var SitemapXML[]
     */
    private array $sitemaps = [];
    
    private ?SitemapIndexXML $sitemapIndex = null;

    /**
     * @inheritDoc
     */
    public function setConfig(ConfigInterface $config): void
    {
        $this->config = $config;
    }

    /**
     * @inheritDoc
     */
    public function addSitemap(string $sitemapName, SitemapXML $sitemapXML): bool
    {
        $this->sitemaps[$sitemapName] = $sitemapXML;
        return true;
    }

    /**
     * @inheritDoc
     */
    public function addSitemapIndex(string $sitemapName, SitemapIndexXML $sitemapIndexXML): bool
    {
        $this->sitemapIndex = $sitemapIndexXML;
        return true;
    }

    /**
     * @return ConfigInterface|null
     */
    public function getConfig(): ?ConfigInterface
    {
        return $this->config;
    }

    /**
     * @return SitemapXML[]
     */
    public function getSitemaps(): array
    {
        return $this->sitemaps;
    }

    /**
     * @return SitemapIndexXML|null
     */
    public function getSitemapIndex(): ?SitemapIndexXML
    {
        return $this->sitemapIndex;
    }

    /**
     * @inheritDoc
     */
    public function getRootDir(): ?string
    {
        return null;
    }
}
