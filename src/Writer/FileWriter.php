<?php

/**
 * Bit&Black Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Sitemap\Writer;

use BitAndBlack\Sitemap\Config\ConfigInterface;
use BitAndBlack\Sitemap\SitemapIndexXML;
use BitAndBlack\Sitemap\SitemapXML;
use RuntimeException;

/**
 * Class FileWriter
 *
 * @package BitAndBlack\Sitemap\Writer
 */
class FileWriter implements WriterInterface
{
    /**
     * FileWriter constructor.
     *
     * @param string $rootDir The directory where all xml file will be stored.
     */
    public function __construct(
        private readonly string $rootDir,
    ) {
        if (!file_exists($this->rootDir)
            && !mkdir($concurrentDirectory = $this->rootDir, 0777, true)
            && !is_dir($concurrentDirectory)
        ) {
            throw new RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
        }
    }

    /**
     * @param ConfigInterface $config
     * @return bool
     */
    public function setConfig(ConfigInterface $config): bool
    {
        $path = $config->getPath();
        
        if (null !== $path
            && !file_exists(dirname($path))
            && !mkdir($concurrentDirectory = dirname($path), 0777, true)
            && !is_dir($concurrentDirectory)
        ) {
            throw new RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
        }

        if (null === $path) {
            return false;
        }
        
        return false !== file_put_contents($path, $config->getConfig());
    }

    /**
     * @param string $sitemapName
     * @param SitemapXML $sitemapXML
     * @return bool
     */
    public function addSitemap(string $sitemapName, SitemapXML $sitemapXML): bool
    {
        return false === file_put_contents(
            $this->rootDir . DIRECTORY_SEPARATOR . $sitemapName,
            (string) $sitemapXML
        );
    }

    /**
     * @param string $sitemapName
     * @param SitemapIndexXML $sitemapIndexXML
     * @return bool
     */
    public function addSitemapIndex(string $sitemapName, SitemapIndexXML $sitemapIndexXML): bool
    {
        return false === file_put_contents(
            $this->rootDir . DIRECTORY_SEPARATOR . $sitemapName,
            (string) $sitemapIndexXML
        );
    }

    /**
     * @return string
     */
    public function getRootDir(): string
    {
        return $this->rootDir;
    }
}
