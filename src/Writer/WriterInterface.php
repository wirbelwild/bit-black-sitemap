<?php

/**
 * Bit&Black Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Sitemap\Writer;

use BitAndBlack\Sitemap\Config\ConfigInterface;
use BitAndBlack\Sitemap\SitemapIndexXML;
use BitAndBlack\Sitemap\SitemapXML;

/**
 * Interface WriterInterface
 *
 * @package BitAndBlack\Sitemap\Writer
 */
interface WriterInterface
{
    /**
     * @return string|null
     */
    public function getRootDir(): ?string;

    /**
     * @return mixed
     */
    public function setConfig(ConfigInterface $config);

    /**
     * Adds the name and the content of a sitemap.
     *
     * @param string $sitemapName                  Name of the sitemap. This may be it's file name.
     * @param SitemapXML $sitemapXML The sitemap's content.
     * @return bool
     */
    public function addSitemap(string $sitemapName, SitemapXML $sitemapXML): bool;

    /**
     * Adds the name and the content of a sitemap index.
     *
     * @param string $sitemapName                            Name of the sitemap index. This may be it's file name.
     * @param SitemapIndexXML $sitemapIndexXML The sitemap index' content.
     * @return bool
     */
    public function addSitemapIndex(string $sitemapName, SitemapIndexXML $sitemapIndexXML): bool;
}
