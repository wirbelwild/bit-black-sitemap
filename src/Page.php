<?php

/**
 * Bit&Black Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Sitemap;

/**
 * @see \BitAndBlack\Sitemap\Tests\PageTest
 */
class Page
{
    private ?string $pageContent = null;

    /**
     * @var array<string, string>
     */
    private array $hrefLangs = [];

    private ?string $languageCode = null;

    /**
     * @var array<string, array<string, string|int|float>>
     */
    private array $links = [];

    /**
     * @var array<string, array<string, string|int|float>>
     */
    private array $images = [];

    private ?int $statusCode = null;

    private ?string $redirection = null;

    private bool $authenticationRequired;

    public function __construct(
        private readonly string $url,
    ) {
        $this->authenticationRequired = false;
    }

    /**
     * @param array{
     *     url: string,
     *     pageContent: string|null,
     *     hrefLangs: array<string, string>,
     *     languageCode: string|null,
     *     links: array<string, array<string, string|int|float>>,
     *     images: array<string, array<string, string|int|float>>,
     *     statusCode: int|null,
     *     redirection: string|null,
     *     authenticationRequired: bool,
     * } $data
     * @return void
     */
    public function __unserialize(array $data): void
    {
        $this->url = $data['url'];
        $this->pageContent = $data['pageContent'];
        $this->hrefLangs = $data['hrefLangs'];
        $this->languageCode = $data['languageCode'];
        $this->links = $data['links'];
        $this->images = $data['images'];
        $this->statusCode = $data['statusCode'];
        $this->redirection = $data['redirection'];
        $this->authenticationRequired = $data['authenticationRequired'];
    }

    public function __serialize(): array
    {
        return [
            'url' => $this->url,
            'pageContent' => $this->pageContent,
            'hrefLangs' => $this->hrefLangs,
            'languageCode' => $this->languageCode,
            'links' => $this->links,
            'images' => $this->images,
            'statusCode' => $this->statusCode,
            'redirection' => $this->redirection,
            'authenticationRequired' => $this->authenticationRequired,
        ];
    }

    /**
     * @return $this
     */
    public function setContent(string $pageContent): self
    {
        $this->pageContent = $pageContent;
        return $this;
    }

    /**
     * @return $this
     */
    public function addHrefLang(string $hreflang, string $href): self
    {
        $this->hrefLangs[$hreflang] = $href;
        return $this;
    }

    /**
     * @return $this
     */
    public function setLanguageCode(string $languageCode): self
    {
        $this->languageCode = $languageCode;
        return $this;
    }

    /**
     * @param array<string, string|int|float> $attributes
     * @return $this
     */
    public function addLink(string $href, array $attributes): self
    {
        $href = AbsoluteUrl::getUrl($href, $this->url);
        $attributes['href'] = $href;
        $this->links[$href] = $attributes;
        return $this;
    }

    /**
     * @param array<string, string|int|float> $attributes
     * @return $this
     */
    public function addImage(string $src, array $attributes): self
    {
        $src = AbsoluteUrl::getUrl($src, $this->url);
        $attributes['src'] = $src;
        $this->images[$src] = $attributes;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return string|null
     */
    public function getPageContent(): ?string
    {
        return $this->pageContent;
    }

    /**
     * @return string[]
     */
    public function getHrefLangs(): array
    {
        return $this->hrefLangs;
    }

    /**
     * @return string|null
     */
    public function getLanguageCode(): ?string
    {
        return $this->languageCode;
    }

    /**
     * @return array<string, array<string, string|int|float>>
     */
    public function getLinks(): array
    {
        return $this->links;
    }

    /**
     * @return array<string, array<string, string|int|float>>
     */
    public function getImages(): array
    {
        return $this->images;
    }

    /**
     * @return Page
     */
    public function setStatusCode(int $statusCode): self
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getStatusCode(): ?int
    {
        return $this->statusCode;
    }

    /**
     * @return $this
     */
    public function setRedirection(string $redirection): self
    {
        $this->redirection = $redirection;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getRedirection(): ?string
    {
        return $this->redirection;
    }

    /**
     * @return $this
     */
    public function setAuthenticationRequired(): self
    {
        $this->authenticationRequired = true;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAuthenticationRequired(): bool
    {
        return $this->authenticationRequired;
    }
}
