<?php

/**
 * Bit&Black Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Sitemap;

/**
 * Class URLParser.
 *
 * @package BitAndBlack\Sitemap
 * @see \BitAndBlack\Sitemap\Tests\URLParserTest
 */
class URLParser
{
    /**
     * This method parses a URL and encodes its parts.
     *
     * @return array{
     *     scheme: ?string,
     *     host: ?string,
     *     port: ?int,
     *     user: ?string,
     *     pass: ?string,
     *     query: ?string,
     *     path: ?string,
     *     fragment: ?string
     * }
     */
    public static function parse(string $url): array
    {
        /** @var array{
         *     scheme: ?string,
         *     host: ?string,
         *     port: ?int,
         *     user: ?string,
         *     pass: ?string,
         *     query: ?string,
         *     path: ?string,
         *     fragment: ?string
         * } $result
         */
        $result = [
            'scheme' => null,
            'host' => null,
            'port' => null,
            'user' => null,
            'pass' => null,
            'query' => null,
            'path' => null,
            'fragment' => null,
        ];

        $replacements = ['!', '*', '\'', '(', ')', ';', ':', '@', '&', '=', '$', ',', '/', '?', '#', '[', ']'];
        $entities = ['%21', '%2A', '%27', '%28', '%29', '%3B', '%3A', '%40', '%26', '%3D', '%24', '%2C', '%2F', '%3F', '%23', '%5B', '%5D'];

        $encodedURL = str_replace($entities, $replacements, urlencode($url));
        $encodedParts = parse_url($encodedURL);

        if (!is_array($encodedParts)) {
            return $result;
        }

        foreach ($encodedParts as $key => $value) {
            $value = str_replace($replacements, $entities, (string) $value);
            $value = urldecode($value);
            $value = rawurldecode($value);
            $result[$key] = $value;
        }

        if (null !== $result['port']) {
            $result['port'] = (int) $result['port'];
        }

        return $result;
    }

    /**
     * This method implodes an array with a parsed URL.
     *
     * @param array<string, mixed> $parsedURL
     * @return string
     */
    public static function unparse(array $parsedURL): string
    {
        $scheme = isset($parsedURL['scheme']) ? $parsedURL['scheme'] . '://' : '';
        $host = $parsedURL['host'] ?? '';
        $port = isset($parsedURL['port']) ? ':' . $parsedURL['port'] : '';
        $user = $parsedURL['user'] ?? '';
        $pass = isset($parsedURL['pass']) ? ':' . $parsedURL['pass'] : '';
        $pass = ($user || $pass) ? $pass . '@' : '';
        $path = $parsedURL['path'] ?? '';
        $query = isset($parsedURL['query']) ? '?' . $parsedURL['query'] : '';
        $fragment = isset($parsedURL['fragment']) ? '#' . $parsedURL['fragment'] : '';
        return $scheme . $user . $pass . $host . $port . $path . $query . $fragment;
    }
}
