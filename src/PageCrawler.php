<?php

/**
 * Bit&Black Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Sitemap;

use BitAndBlack\Helpers\StringHelper;
use BitAndBlack\Helpers\XMLHelper;
use BitAndBlack\Sitemap\Exception\NoPageCrawlerAvailableException;
use BitAndBlack\Sitemap\PageCrawler\AutoPageCrawler;
use BitAndBlack\Sitemap\PageCrawler\PageCrawlerInterface;
use DOMAttr;
use DOMDocument;
use DOMElement;
use DOMNode;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @see \BitAndBlack\Sitemap\Tests\PageCrawlerTest
 */
class PageCrawler
{
    private readonly string $mainURL;

    private readonly LoggerInterface $logger;

    private readonly OutputInterface $output;

    private readonly UrlProcessor $urlProcessor;

    private PageCrawlerInterface $pageCrawler;

    public function __construct(
        private readonly string $url,
    ) {
        $urlParsed = URLParser::parse($this->url);
        $mainURL = $urlParsed['scheme'] . '://' . $urlParsed['host'];
        $this->mainURL = $mainURL;

        $this->urlProcessor = new UrlProcessor($this->mainURL);
        $this->logger = new NullLogger();
        $this->output = new NullOutput();

        $this->pageCrawler = new AutoPageCrawler();
    }

    public static function create(string $url): self
    {
        return new self($url);
    }

    /**
     * @throws NoPageCrawlerAvailableException
     */
    public function getPage(): Page
    {
        $this->logger->debug('Handling url ' . $this->url);
        $this->output->writeln('Handling url <info>' . $this->url . '</info>');

        $page = new Page($this->url);

        $response = $this->pageCrawler->requestUrl($this->url);

        $headers = $response->getHeaders();

        if ([] === $headers) {
            $this->logger->debug('Could not parse ' . var_export($this->url, true));
        }

        $statusCode = $response->getStatusCode();

        $page->setStatusCode($statusCode);

        if (404 === $statusCode) {
            $this->logger->debug(var_export($this->url, true) . ' is dead');
        }

        if (isset($headers['Location'][0])) {
            $locationRedirect = $headers['Location'];

            if (is_array($locationRedirect)) {
                $locationRedirect = end($locationRedirect);
            }

            if (null === URLParser::parse($locationRedirect)['host']) {
                $locationRedirect = $this->mainURL . URLParser::parse($locationRedirect)['path'];
            }

            $this->logger->debug('Found location ' . var_export($locationRedirect, true));

            $page->setRedirection($locationRedirect);
        }

        if (isset($headers['WWW-Authenticate'][0])) {
            $this->logger->debug('This url requires authentication');
            $page->setAuthenticationRequired();
        }

        // Ignore wrong links
        if ($this->urlProcessor->isUrlIgnored($this->url)) {
            $this->logger->debug(var_export($this->url, true) . ' is getting ignored');
        }

        // Ignore external links
        if ($this->urlProcessor->isUrlExternal($this->url)) {
            $this->logger->debug(var_export($this->url, true) . ' is external');
        }

        $pageContent = $response->getBody();

        $page->setContent($pageContent);

        $domDocument = new DOMDocument();
        XMLHelper::loadHTML($domDocument, $pageContent);

        // Get language code of page
        $links = $domDocument->getElementsByTagName('link');
        $this->logger->debug(var_export($links->length, true) . ' links found');

        /**
         * @var DOMElement $element
         */
        foreach ($links as $element) {
            if ($element->hasAttribute('rel')
                && $element->getAttribute('rel') === 'alternate'
                && $element->hasAttribute('hreflang')
            ) {
                $hreflang = $element->getAttribute('hreflang');
                $href = mb_strtolower($element->getAttribute('href'));
                $hrefPath = (string) URLParser::parse($href)['path'];
                $href = $this->mainURL . '/' . ltrim($hrefPath, '/');
                $page->addHrefLang($hreflang, $href);
            }
        }

        // Search for language code in html tag.
        $html = $domDocument->getElementsByTagName('html');

        foreach ($html as $element) {
            if ($element->hasAttribute('lang')) {
                $lang = mb_strtolower($element->getAttribute('lang'));
                $page->setLanguageCode($lang);
            }
        }

        // Get all a tags
        $anchors = $domDocument->getElementsByTagName('a');

        $this->logger->debug(var_export($anchors->length, true) . ' anchors found');

        /**
         * @var DomNode $anchor
         */
        foreach ($anchors as $anchor) {
            if (!$anchor instanceof DOMElement) {
                continue;
            }

            $href = $anchor->getAttribute('href');

            // Remove # and /
            $href = trim($href, '/#');

            $parsed = URLParser::parse($href);

            $parsed['query'] = null;
            $parsed['fragment'] = null;

            $href = URLParser::unparse($parsed);

            if ('' === $href) {
                continue;
            }

            $page->addLink($href, $this->getAttributes($anchor));
        }

        /**
         * Gets all images
         */
        $images = [];
        $imagesAllFound = [];

        $imgElements = $domDocument->getElementsByTagName('img');
        $imgElements = iterator_to_array($imgElements);

        if ([] !== $imgElements) {
            array_push($imagesAllFound, ...$imgElements);
        }

        $sourceElements = $domDocument->getElementsByTagName('source');
        $sourceElements = iterator_to_array($sourceElements);

        if ([] !== $sourceElements) {
            array_push($imagesAllFound, ...$sourceElements);
        }

        foreach ($imagesAllFound as $image) {
            $src = $this->getSrcFromElement($image);

            if (null === $src) {
                continue;
            }

            if (!array_key_exists($src, $images)) {
                $images[$src] = $image;
            }
        }

        $this->logger->debug(var_export(count($images), true) . ' images found');

        foreach ($images as $element) {
            $src = $this->getSrcFromElement($element);

            if (null === $src) {
                continue;
            }

            if ($this->urlProcessor->isUrlImage($src)) {
                $page->addImage($src, $this->getAttributes($element));
            }
        }

        return $page;
    }

    /**
     * @return string|null
     */
    private function getSrcFromElement(DOMElement $element): ?string
    {
        if ($element->hasAttribute('srcset')) {
            return $element->getAttribute('srcset');
        }

        if ($element->hasAttribute('src')) {
            return $element->getAttribute('src');
        }

        return null;
    }

    /**
     * @return array<string, string|int|float>
     */
    private function getAttributes(DOMNode $anchor): array
    {
        $attributes = [];

        /**
         * @var DOMAttr $attribute
         */
        foreach ($anchor->attributes ?? [] as $attribute) {
            $value = $attribute->nodeValue;
            /** @var string|int|float $value */
            $value = StringHelper::stringToNumber($value);
            $attributes[$attribute->name] = $value;
        }

        return $attributes;
    }

    /**
     * @return PageCrawlerInterface
     */
    public function getPageCrawler(): PageCrawlerInterface
    {
        return $this->pageCrawler;
    }

    /**
     * @return PageCrawler
     */
    public function setPageCrawler(PageCrawlerInterface $pageCrawler): self
    {
        $this->pageCrawler = $pageCrawler;
        return $this;
    }
}
