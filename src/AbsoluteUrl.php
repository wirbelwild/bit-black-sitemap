<?php

/**
 * Bit&Black Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Sitemap;

/**
 * @see \BitAndBlack\Sitemap\Tests\AbsoluteUrlTest
 */
class AbsoluteUrl
{
    /**
     * Builds an absolute URL.
     *
     * @param string $link           The link or anchor href.
     * @param string $currentPageURL The URL of the current page.
     * @return string
     */
    public static function getUrl(string $link, string $currentPageURL): string
    {
        if (str_starts_with($link, 'http')) {
            $linkParsed = URLParser::parse($link);
            return URLParser::unparse($linkParsed);
        }

        $path = '/' . ltrim($link, '/');

        $parts = URLParser::parse($currentPageURL);

        $scheme = $parts['scheme'] ?? 'https';
        $host = $parts['host'] ?? '';

        $link = $scheme . '://';

        if (isset($parts['user'], $parts['pass'])) {
            $link .= $parts['user'] . ':' . $parts['pass'] . '@';
        }

        $link .= $host;

        if (isset($parts['port'])) {
            $link .= ':' . $parts['port'];
        }

        /**
         * If the path is an anchor or has parameters, the current page will be added to the url too.
         */
        $pathPart = $path[1] ?? null;

        if ('#' === $pathPart || '?' === $pathPart) {
            $link .= $parts['path'] ?? '';
            $path = ltrim($path, '/');
        }

        $link .= $path;

        return $link;
    }
}
