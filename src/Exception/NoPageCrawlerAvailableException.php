<?php

/**
 * Bit&Black Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Sitemap\Exception;

use BitAndBlack\Sitemap\Exception;

class NoPageCrawlerAvailableException extends Exception
{
    public function __construct()
    {
        parent::__construct('There is no page crawler available on your system.');
    }
}
