<?php

/**
 * Bit&Black Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Sitemap\Exception;

use BitAndBlack\Sitemap\Exception;

/**
 * Class CrawlingNotPossibleException
 *
 * @package BitAndBlack\Sitema\Exception
 */
class CrawlingNotPossibleException extends Exception
{
    /**
     * CrawlingNotPossibleException constructor.
     */
    public function __construct()
    {
        parent::__construct('Crawling is not possible. Please enable curl.');
    }
}
