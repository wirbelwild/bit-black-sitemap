<?php

/**
 * Bit&Black Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Sitemap\Exception;

use BitAndBlack\Sitemap\Exception;

class DependencyNotInstalledException extends Exception
{
    /**
     * BrowserShotNotInstalledException constructor.
     */
    public function __construct(string $name)
    {
        parent::__construct('Could not find `' . $name . '`. Please make sure to have it installed.');
    }
}
