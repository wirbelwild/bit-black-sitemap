<?php

/**
 * Bit&Black Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Sitemap;

/**
 * Class UrlProcessor
 *
 * @package BitAndBlack\Sitemap
 * @see \BitAndBlack\Sitemap\Tests\UrlProcessorTest
 */
class UrlProcessor
{
    /**
     * Links that won't get followed
     *
     * @var array<int, string>
     */
    private array $ignoredLinks = [
        'data:',
        'javascript:',
        'mailto:',
        'tel:',
        '.zip',
        '.pdf',
    ];

    /**
     * Allowed image formats
     *
     * @var array<int, string>
     */
    private array $imageFormats = [
        'jpg',
        'jpeg',
        'png',
        'gif',
        'webp',
        'avif',
    ];

    /**
     * UrlProcessor constructor.
     */
    public function __construct(
        private readonly string $mainURL,
    ) {
    }

    /**
     * Return if url is external
     *
     * @return bool
     */
    public function isUrlExternal(string $url): bool
    {
        return URLParser::parse($url)['host'] !== URLParser::parse($this->mainURL)['host'];
    }

    /**
     * Return if url should be ignored
     *
     * @return bool
     */
    public function isUrlIgnored(string $url): bool
    {
        $isUrlIgnored = false;

        foreach ($this->ignoredLinks as $ignoredLink) {
            if (str_contains($url, $ignoredLink)) {
                $isUrlIgnored = true;
                break;
            }
        }

        return $isUrlIgnored;
    }

    /**
     * Return if url is an image
     *
     * @return bool
     */
    public function isUrlImage(string $url): bool
    {
        $pathinfo = pathinfo($url);

        if (!isset($pathinfo['extension'])) {
            return false;
        }

        $extension = $pathinfo['extension'];
        return in_array($extension, $this->imageFormats, false);
    }
}
