# Changes in Bit&Black Sitemap v2.0

## 2.0.6 2024-03-01

### Fixed

-   Fix missing parameter in serialized page objects.


## 2.0.5 2024-03-01

### Fixed 

-   Fix missing page objects after running unfinished process again. 

## 2.0.4 2024-02-08

### Fixed

-   Strip unneeded url queries.

## 2.0.3 2024-02-07

### Fixed

-   Strip unneeded url fragments.

## 2.0.2 2024-02-07

### Fixed

-   Fixed creation of absolute urls.

## 2.0.1 2024-02-05

### Fixed

-   Added missing dependencies.

## 2.0.0 2024-02-05

### Changed

-   PHP >=8.2 is now required.
-   A lot of code has been refactored: 
    -   The [PageCrawler](./src/PageCrawler.php) is made for crawling an existing web page (see [example1.php](./example/example1.php)).
    -   A crawled web page will be returned as [Page](./src/Page.php). 
    -   The [Page](./src/Page.php) and the [SitemapXML](./src/SitemapXML.php) classes can be initialized manually (see [example2.php](./example/example2.php)).