<?php

/**
 * Bit&Black Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

use BitAndBlack\Sitemap\Collection;
use BitAndBlack\Sitemap\Config\YamlConfig;
use BitAndBlack\Sitemap\Page;
use BitAndBlack\Sitemap\SitemapXML;

require_once dirname(__DIR__) . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

$collection = new Collection(
    new YamlConfig()
);

$page1 = new Page('https://example.org');

$page2 = new Page('https://example.org/contact.php');
$page2->addImage('/about-us.jpg', [
    'title' => 'That\'s us!',
]);

$pages = [
    $page1,
    $page2,
];

$sitemapXML = new SitemapXML($collection, $pages);

var_dump($sitemapXML->getSitemap()->saveXML());
