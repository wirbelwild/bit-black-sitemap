<?php

/**
 * Bit&Black Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

use BitAndBlack\Sitemap\PageCrawler;

require_once dirname(__DIR__) . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

$page = PageCrawler::create('https://www.bitandblack.com/de.html')
    ->getPage()
;

var_dump($page);
