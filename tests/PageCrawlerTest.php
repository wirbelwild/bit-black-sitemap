<?php

/**
 * Bit&Black Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Sitemap\Tests;

use BitAndBlack\Sitemap\Exception\NoPageCrawlerAvailableException;
use BitAndBlack\Sitemap\PageCrawler;
use PHPUnit\Framework\TestCase;

class PageCrawlerTest extends TestCase
{
    /**
     * @throws NoPageCrawlerAvailableException
     */
    public function testCanStripFragments(): void
    {
        $pageCrawler = new PageCrawler('https://www.bitandblack.com/de/systeme.html');
        $page = $pageCrawler->getPage();

        foreach ($page->getLinks() as $link) {
            self::assertStringNotContainsString(
                '#',
                (string) $link['href']
            );
        }
    }

    /**
     * @throws NoPageCrawlerAvailableException
     */
    public function testCanStripQuery(): void
    {
        $pageCrawler = new PageCrawler('https://www.siegertypen-design.de/schrift-des-monats/freight.html');
        $page = $pageCrawler->getPage();

        foreach ($page->getLinks() as $link) {
            self::assertStringNotContainsString(
                '?',
                (string) $link['href']
            );
        }
    }
}
