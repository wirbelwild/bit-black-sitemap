<?php

/**
 * Bit&Black Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Sitemap\Tests;

use BitAndBlack\Sitemap\AbsoluteUrl;
use Generator;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class AbsoluteUrlTest extends TestCase
{
    public static function getGetUrlData(): Generator
    {
        yield [
            'https://www.bitandblack.com/en/imprint.html',
            '/build/images/test.jpg',
            'https://www.bitandblack.com/build/images/test.jpg',
        ];
    }

    /**
     * @return void
     */
    #[DataProvider('getGetUrlData')]
    public function testGetUrl(string $currentPageURL, string $link, string $urlExpected): void
    {
        $url = AbsoluteUrl::getUrl(
            $link,
            $currentPageURL,
        );

        self::assertSame(
            $urlExpected,
            $url
        );
    }
}
