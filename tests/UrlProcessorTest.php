<?php

/**
 * Bit&Black Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Sitemap\Tests;

use BitAndBlack\Sitemap\UrlProcessor;
use Generator;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

/**
 * Class UrlProcessorTest
 *
 * @package BitAndBlack\Sitemap\Tests
 */
class UrlProcessorTest extends TestCase
{
    private UrlProcessor $urlProcessor;

    public static function getIsUrlExternalData(): Generator
    {
        yield [
            'http://www.test.de/something',
            false,
        ];

        yield [
            'http://www.othertest.de/something',
            true,
        ];
    }

    public static function getIsUrlIgnoredData(): Generator
    {
        yield [
            '/something.html',
            false,
        ];

        yield [
            'mailto:hello@you.com',
            true,
        ];

        yield [
            'data:text/calendar;base64,QkVHSU46VkNBTEVOREFSDQpWRVJTSU9OOjIuMA0KUFJPRElEOnNwYXRpZS9pY2FsZW5kYXItZ2VuZXJhdG9yDQpOQU1FOkRSVVBBDQpYLVdSLUNBTE5BTUU6RFJVUEENCkJFR0lOOlZUSU1FWk9ORQ0KVFpJRDpFdXJvcGUvQmVybGluDQpCRUdJTjpTVEFOREFSRA0KRFRTVEFSVDoyMDIzMTAyOVQwMzAwMDANClRaT0ZGU0VURlJPTTorMDIwMA0KVFpPRkZTRVRUTzorMDEwMA0KRU5EOlNUQU5EQVJEDQpCRUdJTjpEQVlMSUdIVA0KRFRTVEFSVDoyMDI0MDMzMVQwMTAwMDANClRaT0ZGU0VURlJPTTorMDEwMA0KVFpPRkZTRVRUTzorMDIwMA0KRU5EOkRBWUxJR0hUDQpCRUdJTjpTVEFOREFSRA0KRFRTVEFSVDoyMDI0MTAyN1QwMzAwMDANClRaT0ZGU0VURlJPTTorMDIwMA0KVFpPRkZTRVRUTzorMDEwMA0KRU5EOlNUQU5EQVJEDQpFTkQ6VlRJTUVaT05FDQpCRUdJTjpWVElNRVpPTkUNClRaSUQ6VVRDDQpCRUdJTjpTVEFOREFSRA0KRFRTVEFSVDoyMDIzMDMxMFQwODI5MDkNClRaT0ZGU0VURlJPTTorMDAwMA0KVFpPRkZTRVRUTzorMDAwMA0KRU5EOlNUQU5EQVJEDQpFTkQ6VlRJTUVaT05FDQpCRUdJTjpWRVZFTlQNClVJRDo2NTZlZGY1NTAwYjQxDQpEVFNUQU1QOjIwMjMxMjA1VDA4MjkwOVoNClNVTU1BUlk6RFJVUEENCkRFU0NSSVBUSU9OOlRvYmlhcyBLw7ZuZ2V0ZXIgdHJlZmZlbiAvIEF1c3N0ZWxsZXIgbWl0IGRlbSBlaWdlbnMgZW50d2lja2VsdA0KIGVuIFNvZnR3YXJlLVN5c3RlbSDCu01hbnlQcmludCBTb2x1dGlvbnPCqy4NCkRUU1RBUlQ7VFpJRD1FdXJvcGUvQmVybGluOjIwMjQwNTI4VDA5MDAwMA0KRFRFTkQ7VFpJRD1FdXJvcGUvQmVybGluOjIwMjQwNjA3VDE3MDAwMA0KRU5EOlZFVkVOVA0KRU5EOlZDQUxFTkRBUg==',
            true,
        ];
    }

    public static function getIsUrlImageData(): Generator
    {
        yield [
            '/something.html',
            false,
        ];

        yield [
            '/something.jpg',
            true,
        ];
    }

    /**
     * UrlProcessorTest constructor.
     */
    protected function setUp(): void
    {
        $this->urlProcessor = new UrlProcessor('http://www.test.de');
    }

    /**
     * @return void
     */
    #[DataProvider('getIsUrlExternalData')]
    public function testIsUrlExternal(string $url, bool $isExternalExpected): void
    {
        self::assertSame(
            $isExternalExpected,
            $this->urlProcessor->isUrlExternal($url)
        );
    }

    /**
     * @return void
     */
    #[DataProvider('getIsUrlIgnoredData')]
    public function testIsUrlIgnored(string $url, bool $isIgnoredExpected): void
    {
        self::assertSame(
            $isIgnoredExpected,
            $this->urlProcessor->isUrlIgnored($url)
        );
    }

    /**
     * @return void
     */
    #[DataProvider('getIsUrlImageData')]
    public function testIsUrlImage(string $url, bool $isImageExpected): void
    {
        self::assertSame(
            $isImageExpected,
            $this->urlProcessor->isUrlImage($url)
        );
    }
}
