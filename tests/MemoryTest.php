<?php

/**
 * Bit&Black Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Sitemap\Tests;

use BitAndBlack\Sitemap\Memory;
use PHPUnit\Framework\TestCase;

/**
 * Class MemoryMest
 *
 * @package BitAndBlack\Tests
 */
class MemoryTest extends TestCase
{
    public function testCanReturnBytes(): void
    {
        $memory = new Memory();
        
        $bytes = $memory->returnBytes('2048M');
        
        self::assertSame(
            (float) (2048 * 1024 * 1024),
            $bytes
        );
    }
}
