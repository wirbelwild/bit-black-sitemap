<?php

/**
 * Bit&Black Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Sitemap\Tests;

use BitAndBlack\Sitemap\URLParser;
use PHPUnit\Framework\TestCase;

/**
 * Class URLParserTest.
 *
 * @package BitAndBlack\Sitemap\Tests
 */
class URLParserTest extends TestCase
{
    public function testCanHandleSpecialChars(): void
    {
        self::assertSame(
            'www.äöüß.de',
            URLParser::parse('https://www.äöüß.de')['host']
        );
        
        self::assertSame(
            '/de-de/land/österreich.html',
            URLParser::parse('https://www.calidar.io/de-de/land/%c3%b6sterreich.html')['path']
        );

        self::assertSame(
            '/en-gb/country/são+tomé+and+príncipe.html',
            URLParser::parse('https://www.calidar.io/en-gb/country/s%c3%a3o+tom%c3%a9+and+pr%c3%adncipe.html')['path']
        );

        self::assertSame(
            '/en-gb/country/são+tomé+and+príncipe.html',
            URLParser::parse('https://www.calidar.io/en-gb/country/são+tomé+and+príncipe.html')['path']
        );
    }
    
    public function testCanParseAndUnparse(): void
    {
        $url = 'https://www.bücher.de/wiröd.html?foo=baß';
        $parsed = URLParser::parse($url);
        
        self::assertSame(
            [
                'scheme' => 'https',
                'host' => 'www.bücher.de',
                'port' => null,
                'user' => null,
                'pass' => null,
                'query' => 'foo=baß',
                'path' => '/wiröd.html',
                'fragment' => null,
            ],
            $parsed
        );

        $unparsed = URLParser::unparse($parsed);

        self::assertSame(
            $unparsed,
            $url
        );
    }
}
