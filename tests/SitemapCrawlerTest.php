<?php

/**
 * Bit&Black Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Sitemap\Tests;

use BitAndBlack\Sitemap\Config\YamlConfig;
use BitAndBlack\Sitemap\SitemapCrawler;
use BitAndBlack\Sitemap\Writer\StringWriter;
use DOMException;
use PHPUnit\Framework\TestCase;

/**
 * Class SitemapCrawlerTest
 *
 * @package BitAndBlack\Sitemap\Tests
 */
class SitemapCrawlerTest extends TestCase
{
    /**
     * @throws DOMException
     */
    public function testCanCrawl(): void
    {
        $config = new YamlConfig(__DIR__ . DIRECTORY_SEPARATOR . 'sitemap.yaml');
        $writer = new StringWriter();

        $sitemapCrawler = new SitemapCrawler($config, $writer);
        $sitemapCrawler->setLogger(new TestLogger());
        $sitemapCrawler->setCrawlingLimit(2);

        $sitemap = $sitemapCrawler->createSitemap('https://www.kiwa.io');

        self::assertIsString(
            $writer->getSitemapIndex()?->getSitemap()->saveXML()
        );

        self::assertArrayHasKey(
            'status',
            $sitemap
        );
        
        self::assertSame(
            'finished',
            $sitemap['status']
        );

        $sitemaps = $writer->getSitemaps();
        
        self::assertNotEmpty($sitemaps);
    }

    /**
     * @throws DOMException
     */
    public function testSetCrawlingLimit(): void
    {
        $config = new YamlConfig(__DIR__ . DIRECTORY_SEPARATOR . 'sitemap.yaml');
        $writer = new StringWriter();
        $sitemapCrawler = new SitemapCrawler($config, $writer);
        $sitemapCrawler->setLogger(new TestLogger());
        $sitemapCrawler->setCrawlingLimit(2);
        
        $sitemap = $sitemapCrawler->createSitemap('https://www.kiwa.io');

        self::assertSame(
            2,
            $sitemap['pages']
        );

        self::assertSame(
            SitemapCrawler::STATUS_FINISHED,
            $sitemap['status']
        );
    }
}
