<?php

/**
 * Bit&Black Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Sitemap\Tests\PageCrawler;

use BitAndBlack\Sitemap\PageCrawler\SymfonyCrawler;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientExceptionInterface;

class SymfonyCrawlerTest extends TestCase
{
    /**
     * @throws ClientExceptionInterface
     */
    public function testRequestUrl(): void
    {
        $symfonyCrawler = new SymfonyCrawler();
        $response = $symfonyCrawler->requestUrl('https://www.bitandblack.com/de.html');

        self::assertStringNotContainsString(
            'text/html; charset=UTF-8',
            (string) $response->getBody()
        );

        self::assertSame(
            200,
            $response->getStatusCode()
        );

        self::assertStringContainsString(
            'Bit&amp;Black',
            (string) $response->getBody()
        );

        self::assertStringContainsString(
            'text/html; charset=UTF-8',
            $response->getHeader('Content-Type')[0] ?? ''
        );
    }
}
