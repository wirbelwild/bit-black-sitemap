<?php

/**
 * Bit&Black Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Sitemap\Tests\PageCrawler;

use BitAndBlack\Sitemap\PageCrawler\BrowserShotCrawler;
use PHPUnit\Framework\TestCase;
use Spatie\Browsershot\Exceptions\FileUrlNotAllowed;

/**
 * Class BrowserShotCrawlerTest.
 *
 * @package BitAndBlack\Sitemap\Tests\PageCrawler
 */
class BrowserShotCrawlerTest extends TestCase
{
    /**
     * @throws FileUrlNotAllowed
     */
    public function testRequestUrl(): void
    {
        if (getenv('CI')) {
            self::markTestSkipped(
                'Skipping test "' . $this->name() . '" as it doesn\'t run in Bitbucket.'
            );
        }

        $guzzleCrawler = new BrowserShotCrawler();
        $response = $guzzleCrawler->requestUrl('https://www.bitandblack.com/de.html');

        self::assertStringNotContainsString(
            'text/html; charset=UTF-8',
            (string) $response->getBody()
        );

        self::assertSame(
            200,
            $response->getStatusCode()
        );

        self::assertStringContainsString(
            'Bit&amp;Black',
            (string) $response->getBody()
        );

        self::assertStringNotContainsString(
            'text/html; charset=UTF-8',
            $response->getHeader('Content-Type')[0] ?? ''
        );

        self::assertStringContainsString(
            '<script type="text/javascript" async="" src="https://matomo.bitandblack.com/matomo.js"></script>',
            (string) $response->getBody()
        );
    }
}
