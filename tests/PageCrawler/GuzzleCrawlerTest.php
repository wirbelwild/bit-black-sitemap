<?php

/**
 * Bit&Black Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Sitemap\Tests\PageCrawler;

use BitAndBlack\Sitemap\PageCrawler\GuzzleCrawler;
use PHPUnit\Framework\TestCase;

class GuzzleCrawlerTest extends TestCase
{
    public function testRequestUrl(): void
    {
        $guzzleCrawler = new GuzzleCrawler();
        $response = $guzzleCrawler->requestUrl('https://www.bitandblack.com/de.html');

        self::assertStringNotContainsString(
            'text/html; charset=UTF-8',
            (string) $response->getBody()
        );

        self::assertSame(
            200,
            $response->getStatusCode()
        );

        self::assertStringContainsString(
            'Bit&amp;Black',
            (string) $response->getBody()
        );

        self::assertStringContainsString(
            'text/html; charset=UTF-8',
            $response->getHeader('Content-Type')[0] ?? ''
        );
    }
}
