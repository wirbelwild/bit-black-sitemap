<?php

/**
 * Bit&Black Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Sitemap\Tests;

use Psr\Log\LoggerInterface;
use Stringable;

/**
 * Class TestLogger
 *
 * @package BitAndBlack\Sitemap\Tests
 */
class TestLogger implements LoggerInterface
{
    /**
     * @param array<mixed> $context
     */
    public function write(string $type, mixed $message, array $context = []): void
    {
        $message = trim(var_export($message, true), '\'');
        fwrite(STDERR, $type . ': ' . $message . ' / ' . json_encode($context, 1) . '' . PHP_EOL);
    }
    
    /**
     * @inheritDoc
     */
    public function emergency(string|Stringable $message, array $context = []): void
    {
        $this->write('Emergency', $message, $context);
    }

    /**
     * @inheritDoc
     */
    public function alert(string|Stringable $message, array $context = []): void
    {
        $this->write('Alert', $message, $context);
    }

    /**
     * @inheritDoc
     */
    public function critical(string|Stringable $message, array $context = []): void
    {
        $this->write('Critical', $message, $context);
    }

    /**
     * @inheritDoc
     */
    public function error(string|Stringable $message, array $context = []): void
    {
        $this->write('Error', $message, $context);
    }

    /**
     * @inheritDoc
     */
    public function warning(string|Stringable $message, array $context = []): void
    {
        $this->write('Warning', $message, $context);
    }

    /**
     * @inheritDoc
     */
    public function notice(string|Stringable $message, array $context = []): void
    {
        $this->write('Notice', $message, $context);
    }

    /**
     * @inheritDoc
     */
    public function info(string|Stringable $message, array $context = []): void
    {
        $this->write('Info', $message, $context);
    }

    /**
     * @inheritDoc
     */
    public function debug(string|Stringable $message, array $context = []): void
    {
        $this->write('Debug', $message, $context);
    }

    /**
     * @inheritDoc
     */
    public function log(mixed $level, string|Stringable $message, array $context = []): void
    {
        $this->write('Log', $message, $context);
    }
}
