<?php

/**
 * Bit&Black Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Sitemap\Tests;

use BitAndBlack\Sitemap\Page;
use PHPUnit\Framework\TestCase;

class PageTest extends TestCase
{
    public function testAddImage(): void
    {
        $mainUrl = 'https://www.bitandblack.com';
        $currentUrl = $mainUrl . '/en/test.html';
        $image = '/image.jpg';
        $urlExpected = $mainUrl . $image;

        $page = new Page($currentUrl);

        $page->addImage($image, [
            'alt' => 'My Alt Text',
        ]);

        $images = $page->getImages();

        self::assertCount(
            1,
            $images
        );

        self::assertSame(
            $urlExpected,
            array_keys($images)[0]
        );

        self::assertSame(
            $urlExpected,
            array_values($images)[0]['src']
        );
    }

    public function testAddLink(): void
    {
        $mainUrl = 'https://www.bitandblack.com';
        $currentUrl = $mainUrl . '/en/test.html';
        $link = '/en/imprint.html';
        $urlExpected = $mainUrl . $link;

        $page = new Page($currentUrl);

        $page->addLink($link, [
            'hreflang' => 'de-de',
        ]);

        $links = $page->getLinks();

        self::assertCount(
            1,
            $links
        );

        self::assertSame(
            $urlExpected,
            array_keys($links)[0]
        );

        self::assertSame(
            'de-de',
            array_values($links)[0]['hreflang']
        );
    }
}
